/* 
    version: v0.5
    скрипт для отключения рации в радиусе
    Применяется из Achilles выполнением кода на объекте
    В переменную _jammerDistance записывается дистанция в метрах, в которой отключаются все каналы кроме приямого и транспортного
 */

private _jammerDistance = 1000;

if (isServer) exitWith {};
waitUntil { not isNull player };

private _radioJammer = _this select 1;
private _jammed = false;

while { alive _radioJammer } do {
    if (player distance _radioJammer < _jammerDistance) then {
        if (!_jammed) then {
            setCurrentChannel 5;
            0 enableChannel [false, false];
            1 enableChannel [false, false];
            2 enableChannel [false, false];
            3 enableChannel [false, false];
            _jammed = true;
        };
    } else {
        if (_jammed) then {
        0 enableChannel [true, true];
        1 enableChannel [true, true];
        2 enableChannel [true, true];
        3 enableChannel [true, true];
        _jammed = false;      
        };
    };
    sleep 3;
};

0 enableChannel [true, true];
1 enableChannel [true, true];
2 enableChannel [true, true];
3 enableChannel [true, true];
